import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PokemonsService } from './pokemons.service';

import { Pokemon } from './pokemon';
import { POKEMONS } from './mock-pokemons';


@Component({
  selector: 'list-pokemon',
  templateUrl: './list-pokemon.component.html',
  providers: [PokemonsService]
})
export class ListPokemonComponent implements OnInit {

  pokemons: Pokemon[] = null;

  constructor(private router: Router, private pokemonsService: PokemonsService) { }

  // list-pokemon.component.ts
  ngOnInit(): void {
    this.getPokemons();
  }

  // APRES
  getPokemons(): void {
    this.pokemonsService.getPokemons().then(pokemons => this.pokemons = pokemons);
  }



  selectPokemon(pokemon: Pokemon): void {
    console.log('Vous avez selectionné ' + pokemon.name);
    let link = ['/pokemon', pokemon.id];
    this.router.navigate(link);
  }

}
