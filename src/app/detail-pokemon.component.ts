import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router, Params } from '@angular/router';
import { Pokemon } from './pokemon';
import { POKEMONS } from './mock-pokemons'; // on peut supprimer cette ligne, nous n'en avons plus besoin.
import { PokemonsService } from './pokemons.service'; // on importe le service PokemonsService.

@Component({
  selector: 'detail-pokemon',
  templateUrl: 'detail-pokemon.component.html',
  providers: [PokemonsService] // on déclare un fournisseur pour le service.
})
export class DetailPokemonComponent implements OnInit {

   pokemon: Pokemon = null;

  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private pokemonsService: PokemonsService) {} // on injecte ce service pour pouvoir l'utiliser dans le composant.

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      this.pokemon = this.pokemonsService.getPokemon(id); // on utilise le service pour récupérer un pokémon en fonction de son identifiant.
    });
  }

  goBack(): void {
    window.history.back();
  }


  // On crée une méthode qui s'occupe de la redirection
  goEdit(pokemon: Pokemon): void {
    let link = ['/pokemon/edit', pokemon.id];
    this.router.navigate(link);
  }
}


